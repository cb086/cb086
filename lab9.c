#include<stdio.h>
int main()
{
    int a,b,*pa=&a,*pb=&b;
    float sum,sub,mul,div,rem,*psum=&sum,*psub=&sub,*pmul=&mul,*pdiv=&div,*prem=&rem;
    printf("Enter the first number=  ");
    scanf("%d",pa);
    printf("Enter the second number=  ");
    scanf("%d",pb);
    sum=*pa+*pb;
    sub=(*pa)-(*pb);
    div=((float)(*pa))/((float)(*pb));
    mul=(*pa)*(*pb);
    rem=(*pa)%(*pb);
    printf("****************************************\n");
    printf("The result after addition=  %.2f\n",*psum);
    printf("The result after subtraction=  %.2f\n",*psub);
    printf("The result after multiplication=  %.2f\n",*pmul);
    printf("The result after division=  %.2f\n",*pdiv);
    printf("The remainder is=  %.2f\n",*prem);
    return 0;
}