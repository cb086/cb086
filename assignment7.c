#include<stdio.h>
int main()
{
	int matrix[5][5];
	int diag=0,belowdia=0,abovedia=0;
	int i,j,n,b=1;
	printf("Enter the number of rows of square matrix=  ");
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		printf("Enter the elements of row %d=  ",i+1);
		for(j=0;j<n;j++)
		{
			scanf("%d",&matrix[i][j]);
		}
	}
	printf("The matrix you have entered is: \n\n");
	for(i=0;i<n;i++)
	{
		printf("| ");
		for(j=0;j<n;j++)
		{
			printf("%d ",matrix[i][j]);
		}
		printf(" |\n");
	}
	for(i=0;i<n;i++)
	{
		diag=diag+matrix[i][i];
	}
	for(i=0;i<n;i++)
	{
		for(j=b;j<n;j++)
		{
			belowdia=belowdia+matrix[j][i];
		}
		b++;
	}
	b=1;
	for(i=0;i<n;i++)
	{
		for(j=b;j<n;j++)
		{
			abovedia=abovedia+matrix[i][j];
		}
		b++;
	}			
	printf("\nThe sum of the diagonal elements of the matrix is :%d\n",diag);
	printf("The sum of the elements below the diagonal:  %d\n",belowdia);
	printf("The sum of the elements above the diagonal:  %d\n",abovedia);
return 0;
}