#include<stdio.h>
int main()
{
	int a,b,*pa=&a,*pb=&b;
	int temp;
	printf("Enter the number a =  ");
	scanf("%d",pa);
	printf("Enter the number b=  ");
	scanf("%d",pb);
	printf("Before swapping:\na= %d\nb= %d \n",*pa,*pb);
	temp=*pa;
	*pa=*pb;
	*pb=temp;
	printf("After swapping:\na= %d\nb= %d\n",*pa,*pb);
	return 0;	
}
